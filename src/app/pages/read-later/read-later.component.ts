import { Component, NgZone, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DbService } from 'src/app/services/db.service';
import { NewsService } from 'src/app/services/news.service';

@Component({
  selector: 'app-read-later',
  templateUrl: './read-later.component.html',
  styleUrls: ['./read-later.component.scss']
})
export class ReadLaterComponent implements OnInit {
  public newsList: any = [];
  constructor(public newsService: NewsService, private loaderService: NgxUiLoaderService, private zone: NgZone, public toastr: ToastrService, public dbService: DbService) { }

  ngOnInit(): void {
    this.loaderService.start();
    this.getReadLater();
  }

  /** get all news added under Read Later list */
  public getReadLater() {
    var temThis = this;
    var userId = localStorage.getItem('userid');
    this.dbService.getReadLater(userId, function (news: any) {

      for (const [key, value] of Object.entries(news)) {
        temThis.zone.run(() => {
          temThis.newsList.push(JSON.parse(value['metadata']));
        })
      }
    });
    this.loaderService.stop();
  }

  /** remove news from Read Later list */
  public onDelete(news) {
    var temThis = this;
    var userId = localStorage.getItem('userid');
    this.dbService.removeFromList(news, userId, function (flag: any) {
      if (flag) {
        temThis.zone.run(() => {
          temThis.toastr.success('Hello user!', 'Removed from the list', {
            positionClass: 'toast-top-center'
          });
          location.reload();
        })
      } else {
        temThis.zone.run(() => {
          temThis.toastr.warning('Hello user!', 'Please try again', {
            positionClass: 'toast-top-center'
          });
        })
      }
    })
  }
}
