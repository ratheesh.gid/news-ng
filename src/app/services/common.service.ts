import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CommonService {
    newyorkTimesApi: string = 'uR1j3A82i48Cvvn6A4pQRWBCIhUCIvG7';  // Api key for fetching newyork time contents
    newsUrl: string = 'https://api.nytimes.com/svc/news/v3/content/all/all.json';  // Api for fetching news 
    sectionUrl: string = 'https://api.nytimes.com/svc/news/v3/content/section-list.json'; //Api for fetching news category(sections)

    constructor() { }

    public getNewsUrl() {
        return this.newsUrl + '?api-key=' + this.newyorkTimesApi;
    }
    public getSectionUrl() {
        return this.sectionUrl + '?api-key=' + this.newyorkTimesApi;
    }

    public setSession(user) {
        localStorage.setItem('loggedin', 'true');
        localStorage.setItem('userid', user.id);
        localStorage.setItem('username', user.name);
    }

    public unsetSession() {
        localStorage.setItem('loggedin', 'false');
        localStorage.setItem('username', '');
        localStorage.setItem('userid', '');
    }
}