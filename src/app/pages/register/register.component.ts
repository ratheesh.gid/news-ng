import { Component, NgZone, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/common.service';
import { DbService } from 'src/app/services/db.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public btnClicked: boolean = false;

  /**
   * ------Validation Set-----
   * name - no special char, required
   * email - format check, required
   * password - no special char, max char length is 20, required
   */
  public registerForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9]+$")]),
    email: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
    password: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9]{1,20}$")]),
    cnfmpassword: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9]{1,20}$")]),
  });
  constructor(public dbService: DbService, public commonService: CommonService, public toastr: ToastrService, private zone: NgZone, public router: Router, private loaderService: NgxUiLoaderService) { }

  ngOnInit(): void {
  }

  /**
   * Checks if the entered email is already linked with another account.
   * @param evt 
   */
  public checkAlready(evt) {
    var temThis = this;
    this.dbService.alreadyExists(this.registerForm.value.email, function (check) {
      if (check) {
        temThis.zone.run(() => {
          temThis.registerForm.get('email').setValue('');
          temThis.toastr.warning('Hello user!', 'User Already Exist', {
            positionClass: 'toast-top-center'
          });
        })
      } 
    });
  }

  /**
   * Registration function
   * Redirect to home if successfully registered.
   */
  public register() {
    this.loaderService.start();
    let data = this.dbService.createUser(this.registerForm.value);
    this.registerForm.reset();
    this.toastr.success('Hello user!', 'Successfully Registered', {
      positionClass: 'toast-top-center'
    });
    this.commonService.setSession(data);
    this.router.navigate(['/home']);
    this.loaderService.stop();
  }
}
