import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {
  activePage: number = 0;
  @Input() recordsPerPage: number = 10; // Max number of news should be displayed on each page
  startsFrom: number = 0; // starting index of news array to be dispalyed in current active page
  endsOn: number = 0; // ending index of news array to be dispalyed in current active page
  @Input() showreadlaterbtn: boolean = true; //show or hide Read Later button
  @Input() showreadDeletebtn: boolean = false; //show or hide Delete button
  public newsList: any = [];

  @Input()
  set newslist(val: any) {
    this.newsList = val;
    this.activePage = 0;
    // this.recordsPerPage = 6; // Max number of news should be displayed on each page
    this.startsFrom = 0; // starting index of news array to be dispalyed in current active page
    this.endsOn = this.recordsPerPage;
  }

  @Output() onReadLater: EventEmitter<any> = new EventEmitter();
  @Output() onDelete: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.endsOn = this.recordsPerPage
  }

  ngOnInit(): void {
  }
  /**
   * 
   * @param activePageNumber Active page number (will update on clicking the pagination navs)
   */
  displayActivePage(activePageNumber: number) {
    this.activePage = activePageNumber;
    this.startsFrom = (this.activePage - 1) * this.recordsPerPage;
    this.endsOn = this.startsFrom + this.recordsPerPage;
  }

  /**
   * 
   * @param news 
   */
  readLater(news: any) {
    this.onReadLater.emit(news);
  }
  /**
   * 
   * @param news 
   */
  public deleteNews(news: any) {
    this.onDelete.emit(news);
  }
}
