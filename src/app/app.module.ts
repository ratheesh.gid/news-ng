import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CommonService } from './services/common.service';
import { NewsService } from './services/news.service';
import { DbService } from './services/db.service';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { HeaderComponent } from './pages/common/header/header.component';
import { SidebarComponent } from './pages/common/sidebar/sidebar.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { NewsListComponent } from './pages/common/news-list/news-list.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ToastrModule } from 'ngx-toastr';
import { NgxUiLoaderModule } from "ngx-ui-loader";
import { PaginationComponent } from './pages/common/pagination/pagination.component';
import { ReadLaterComponent } from './pages/read-later/read-later.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    SidebarComponent,
    ProfileComponent,
    NewsListComponent,
    PaginationComponent,
    ReadLaterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgScrollbarModule,
    BrowserAnimationsModule,
    NgxUiLoaderModule,
    ToastrModule.forRoot()
  ],
  providers: [DbService, CommonService, NewsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
