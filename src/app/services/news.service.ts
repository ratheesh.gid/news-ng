import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonService } from './common.service';

@Injectable({
    providedIn: 'root'
})
export class NewsService {
    newsUrl: string = '';
    sectionUrl: string = '';

    constructor(private httpClient: HttpClient, public commonService: CommonService) {
        this.newsUrl = this.commonService.getNewsUrl();
        this.sectionUrl = this.commonService.getSectionUrl();
    }

    /**
     * Get all news
     */
    public getNews() {
        return this.httpClient.get(`${this.newsUrl}`);
    }

    /**
     * Get all news sections
     */
    public getSections() {
        return this.httpClient.get(`${this.sectionUrl}`);
    }
}