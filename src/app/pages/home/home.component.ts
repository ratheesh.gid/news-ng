import { Component, NgZone, OnInit } from '@angular/core';
import { NewsService } from 'src/app/services/news.service';
import { NgxUiLoaderService } from "ngx-ui-loader";
import { DbService } from 'src/app/services/db.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public activeSection: string = '';

  public newsAllList: any = [];
  public newsList: any = [];
  public sectionsList: any = [];

  public showMobSection: boolean = false;

  constructor(public newsService: NewsService, private loaderService: NgxUiLoaderService, private zone: NgZone, public toastr: ToastrService, public dbService: DbService) { }

  ngOnInit(): void {
    this.loaderService.start(); // Start spinner
    this.getAllSection();
  }

  /** get all news */
  public getAllNews() {
    this.newsService.getNews().subscribe((res: any) => {
      this.newsAllList = res;
      this.newsSectionWise();
    }, (err: any) => {

    });
  }

  /** get all news sections */
  public getAllSection() {
    this.newsService.getSections().subscribe((res: any) => {
      if (res.results) {
        this.sectionsList = res.results;
        this.activeSection = 'All Sections'; //this.sectionsList[0].display_name;
        this.getAllNews();
      }
    }, (err: any) => {

    });
  }

  /** emits on sidebar item click  */
  public sectionChange(index) {
    if (index !== -1) {
      this.activeSection = this.sectionsList[index].display_name;
    } else {
      this.activeSection = 'All Sections';
    }
    this.newsSectionWise();
  }

  /** filter the news by active section */
  public newsSectionWise() {
    let arr = [];
    if (this.activeSection === 'All Sections') {
      this.newsList = this.newsAllList.results;
    } else {
      this.newsAllList.results.forEach(element => {
        if (element.section === this.activeSection) {
          arr.push(element);
        }
      });
      this.newsList = arr;
    }

    this.loaderService.stop(); //stop spinner
  }

  /** Add news to Read Later List */
  public onReadLater(news) {
    var temThis = this;
    var userId = localStorage.getItem('userid');
    this.dbService.readLater(userId, news, function (flag) {
      if (flag) {
        temThis.zone.run(() => {
          temThis.toastr.success('Hello user!', 'Added to Read Later list', {
            positionClass: 'toast-top-center'
          });
        })
      } else {
        temThis.zone.run(() => {
          temThis.toastr.warning('Hello user!', 'Already added in the list', {
            positionClass: 'toast-top-center'
          });
        })
      }
    });
  }
}
