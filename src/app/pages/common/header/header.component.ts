import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isCollapsed: boolean = true;
  userLogged: boolean = false;
  constructor(public router: Router, public commonService: CommonService) { }

  ngOnInit(): void {
    console.log(localStorage.getItem('adminlogged'));
    if (localStorage.getItem('loggedin') !== 'true') {
      this.userLogged = false;
      this.router.navigate(['/login']);
    } else {
      this.userLogged = true;
    }
  }

  public logout() {
    this.commonService.unsetSession();
    this.router.navigate(['/login']);
  }

}
