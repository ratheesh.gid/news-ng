import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DbService } from 'src/app/services/db.service';
import { NewsService } from 'src/app/services/news.service';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public userName: string = '';

  /**
   * ------Validation Set-----
   * password - no special char, max char length is 20, required
   */
  public changePasswordForm = new FormGroup({
    password: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9]{1,20}$")]),
    newpassword: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9]{1,20}$")]),
    cnfmpassword: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9]{1,20}$")])
  });

  public btnClicked: boolean = false;

  @ViewChild('content') content: ElementRef;
  constructor(public newsService: NewsService, public commonService: CommonService, private loaderService: NgxUiLoaderService, private zone: NgZone, public toastr: ToastrService, public dbService: DbService, public modalService: NgbModal, public router: Router) { }
  ngOnInit(): void {
    this.userName = localStorage.getItem('username');
  }

  /** Display Name change */
  public changename() {
    if (this.userName !== '') {
      var res = this.dbService.updateUser(this.userName, localStorage.getItem('userid'));
      if (res) {
        this.toastr.success('Hello user!', 'Successfully Updated', {
          positionClass: 'toast-top-center'
        });
        localStorage.setItem('username', this.userName);

      }
    }
  }

  /** Password change */
  public changePass() {
    var temThis = this;
    temThis.loaderService.start();
    this.dbService.changePassword(this.changePasswordForm.value, localStorage.getItem('userid'), function (flag) {
      if (flag) {
        temThis.zone.run(() => {
          temThis.toastr.success('Hello user!', 'Password changed', {
            positionClass: 'toast-top-center'
          });
          temThis.loaderService.stop();
        })
      } else {
        temThis.zone.run(() => {
          temThis.toastr.warning('Hello user!', 'Incorrect Current Password', {
            positionClass: 'toast-top-center'
          });
          temThis.loaderService.stop();
        })
      }
    });
  }

  /** Delete Account */
  public deleteAccount() {
    var temThis = this;
    // confirmation popup //
    this.modalService.open(this.content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      if (result === 'yes') {
        temThis.loaderService.start();
        this.dbService.deleteUser(localStorage.getItem('userid'), function (flag) {
          if (flag) {
            temThis.zone.run(() => {
              temThis.toastr.success('Hello user!', 'Account Deleted', {
                positionClass: 'toast-top-center'
              });
              temThis.logout();
              temThis.loaderService.stop();
            })
          } else {
            temThis.zone.run(() => {
              temThis.toastr.warning('Hello user!', 'Incorrect Current Password', {
                positionClass: 'toast-top-center'
              });
              temThis.loaderService.stop();
            })
          }
        });
      }
    }, (reason) => {
    });
  }

  /** triggers logout once account is deleted */
  public logout() {
    this.commonService.unsetSession();
    this.router.navigate(['/login']);
  }

}
