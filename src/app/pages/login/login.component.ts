import { Component, NgZone, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/common.service';
import { DbService } from 'src/app/services/db.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public btnClicked: boolean = false;
  /**
   * ------Validation Set-----
   * email - format check, required
   * password - no special char, max char length is 20, required
   */
  public loginForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
    password: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9]{1,20}$")])
  });
  constructor(public dbService: DbService, public commonService: CommonService, public router: Router, private zone: NgZone, public toastr: ToastrService, private loaderService: NgxUiLoaderService) { }

  ngOnInit(): void {
  }

  /**
   * Login function
   */
  public login() {
    this.loaderService.start();
    let tempThis = this;
    this.dbService.userLogin(this.loginForm.value, function (user) {
      console.log(user)
      if (!user) {
        tempThis.toastr.warning('Hello user!', 'Invalid Credentials', {
          positionClass: 'toast-top-center'
        });
        tempThis.loaderService.stop();
      } else {
        tempThis.zone.run(() => {
          tempThis.commonService.setSession(user);
          tempThis.router.navigate(['/home']);
        });
        tempThis.loaderService.stop();
      }
    });
  }

}
