import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgScrollbar } from 'ngx-scrollbar';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public sectionList: any = [];
  public activeMenu: string = 'All Sections';
  @Input()
  set sectionlist(val: any) {
    if (val.length) {
      this.sectionList = val;
      // this.activeMenu = this.sectionList[0].section;
    }
  }

  @Output() sectionChange: EventEmitter<any> = new EventEmitter();
  constructor() {   }

  ngOnInit(): void {
  }
  /** emits section click event */
  sectionClick(evt) {
    this.sectionChange.emit(evt);
  }
}
