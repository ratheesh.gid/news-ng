import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonService } from './common.service';
@Injectable({
    providedIn: 'root'
})
export class DbService {
    /**
     * Browser storage (Web sql) is used for handling data process locally. 
     * 
     */
    public db: any;
    constructor(private httpClient: HttpClient, public commonService: CommonService) { }

    public getDbInstance() {
        /**
         * The openDatabase method takes care of opening a database if it already exists, this method will create it if it already does not exist.
         */
        this.db = (<any>window).openDatabase('newsDb', '1.0', 'News portal web sql', 2 * 1024 * 1024);
        this.db.transaction(function (tx) {
            /**
             * USERS and READLATER are the tables used for this application
             * Here we were creating the tables if it is not exist
             */
            tx.executeSql('CREATE TABLE IF NOT EXISTS USERS (id unique, name, email, password)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS READLATER (id unique, title, slug, userid, metadata)');
        });
    }

    /**
     * Insert new user data in to the USERS table 
     */
    public createUser(data) {
        const min = Math.ceil(100);
        const max = Math.floor(10000);
        const uId = Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive

        this.db.transaction(function (tx) {
            tx.executeSql('INSERT INTO USERS (id, name, email, password) VALUES (?, ?, ?, ?)', [uId, data.name, data.email, data.password]);
        });
        data['id'] = uId;
        return data;
    }

    /**
     * Update user display name
     * @param name 
     * @param id 
     */
    public updateUser(name, id) {
        this.db.transaction(function (tx) {
            tx.executeSql('UPDATE USERS SET name=? WHERE id=?', [name, parseInt(id)]);
        });
        return true;
    }

    /**
     * Delete user record.
     * @param userId 
     * @param callBack 
     */
    public deleteUser(userId, callBack) {
        var flag = false;
        this.db.transaction(function (tx) {
            tx.executeSql('DELETE FROM USERS WHERE id = ?', [parseInt(userId)], function (tx, results) {
                flag = true;
                callBack(flag);
            }, null);
        });
    }

    /**
     * Change user password
     * @param data 
     * @param userId 
     * @param callBack 
     */
    public changePassword(data, userId, callBack) {
        var flag = false;
        this.db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM USERS WHERE id=? AND password=?', [parseInt(userId), data.password], function (tx, results) {
                var len = results.rows.length;
                console.log(results);
                if (len > 0) {
                    tx.executeSql('UPDATE USERS SET password=? WHERE id=?', [data.newpassword, parseInt(userId)]);
                    flag = true;
                } else {
                    flag = false;
                }
                callBack(flag);
            }, null);
        });
    }

    /**
     * checks email already exists in USERS table
     */
    public alreadyExists(emailId, callBack) {
        var isExist;
        this.db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM USERS WHERE email = ?', [emailId], function (tx, results) {
                var len = results.rows.length;
                if (len > 0) {
                    isExist = true;
                } else {
                    isExist = false;
                }
                callBack(isExist);
            }, null);
        });
    }

    /**
     * Login function
     */
    public userLogin(data, callBack) {
        var user;
        this.db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM USERS WHERE email = ? AND password = ?', [data.email, data.password], function (tx, results) {
                var len = results.rows.length;
                if (len > 0) {
                    user = results.rows[0];
                } else {
                    user = false;
                }
                callBack(user);
            }, null);
        });

    }

    /**
     * Insert news into Read Later list
     */
    public readLater(userId, data, callBack) {
        const min = Math.ceil(100);
        const max = Math.floor(10000);
        const uId = Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
        var flag
        this.db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM READLATER WHERE slug = ? AND userid = ?', [data.slug_name, userId], function (tx, results) {
                var len = results.rows.length;
                if (len > 0) {
                    flag = false;
                } else {
                    tx.executeSql('INSERT INTO READLATER (id, title, slug, userid, metadata) VALUES (?, ?, ?, ?, ?)', [uId, data.title, data.slug_name, userId, JSON.stringify(data)]);

                    flag = true;
                }
                callBack(flag);
            }, null);
        });
    }

    /**
     * Get Read Later list
     * @param userId 
     * @param callBack 
     */
    public getReadLater(userId, callBack) {
        var news;
        this.db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM READLATER WHERE userid = ?', [userId], function (tx, results) {
                var len = results.rows.length;
                if (len > 0) {
                    news = results.rows;
                } else {
                    news = [];
                }
                callBack(news);
            }, null);
        })
    }

    /**
     * Remove news for Read Later list
     */
    public removeFromList(news, userId, callBack) {
        var flag = false;
        this.db.transaction(function (tx) {
            tx.executeSql('DELETE FROM READLATER WHERE userid = ? AND slug = ?', [userId, news.slug_name], function (tx, results) {
                flag = true;
                callBack(flag);
            }, null);
        });
    }

}